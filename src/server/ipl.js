function getNoOfMatchesPerYear(matchesData){
    try{
        let matchSeasons = matchesData.map(function(matchInfo) 
                        { return matchInfo["season"]; });
        
        
        let noOfMatchesPerYear = matchSeasons.reduce(function (allMatchesCount, season) { 
            if (season in allMatchesCount) {
              allMatchesCount[season]++
            }
            else {
                allMatchesCount[season]=1
            }
            return allMatchesCount
          }, {})
        
        

        return noOfMatchesPerYear;
    }
    catch(err){
        console.error(err);
    }
}

function getNoOfWonMatchesPerTeamPerYear(matchData){
    try{
        
        let noOfWonMatchesPerTeamPerYear=matchData.reduce((matchInfo,currentMatch)=>{
            if(matchInfo.hasOwnProperty(currentMatch['season'])){
                if(matchInfo[currentMatch.season].hasOwnProperty(currentMatch['winner'])){
                    matchInfo[currentMatch.season][currentMatch.winner]+=1
                }
                else{
                    matchInfo[currentMatch.season][currentMatch.winner]=1
                }
            }
            else{
                matchInfo[currentMatch.season]={}
                matchInfo[currentMatch.season][currentMatch.winner]=1
            }
            return matchInfo
        },{})
        
        return noOfWonMatchesPerTeamPerYear;
        
    }
    catch(err){
        console.error(err);
    }
}

function getExtraRunsPerTeam(matchesData,deliveriesData){
    try{
        
        let matchId2016=matchesData.filter((matchInfo)=>{return matchInfo['season']==2016}).map((matchInfo)=> {return matchInfo['id']});;
        let extraRunsPerTeamIn2016=deliveriesData.reduce((extraRunsObj,currentDelivery)=>{
            if(matchId2016.includes(currentDelivery['match_id'])){
                if(extraRunsObj.hasOwnProperty(currentDelivery['batting_team'])){
                    extraRunsObj[currentDelivery['batting_team']]+=Number(currentDelivery['extra_runs']);
                }
                else{
                    extraRunsObj[currentDelivery['batting_team']]=Number(currentDelivery['extra_runs']);
                }
                
            }
            return extraRunsObj;
        },{})
        
        
        return extraRunsPerTeamIn2016;
        
    }
    catch(err){
        console.error(err);
    }
}

function getTopTenEconomicalBowler(matchesData,deliveriesData){
    try{
        let matchId2015 =matchesData.filter((currentMatch)=>{return currentMatch['season']==2015}).map((currentMatch)=> {return currentMatch['id']});;
        
        let  bowlersRunsAndOvers=deliveriesData.reduce((economyObj,currentDelivery)=>{
            if(matchId2015.includes(currentDelivery['match_id'])){
                if(economyObj.hasOwnProperty(currentDelivery['bowler'])){
                    if(currentDelivery['ball']==1){
                        economyObj[currentDelivery['bowler']][1]+=1;
                    }
                    economyObj[currentDelivery['bowler']][0] += Number(currentDelivery['total_runs'])-currentDelivery['legbye_runs']-currentDelivery['bye_runs']
                }
                else{
                    economyObj[currentDelivery['bowler']]=[]
                    economyObj[currentDelivery['bowler']][0]=Number(currentDelivery['total_runs'])-currentDelivery['legbye_runs']-currentDelivery['bye_runs']
                    economyObj[currentDelivery['bowler']][1]=1
                }
            }
            return economyObj
        },{})
        bowlersRunsAndOvers=Object.entries(bowlersRunsAndOvers);

        let bowlersEconomy = bowlersRunsAndOvers.reduce((bowlerEconomy,currentBowler)=>{
            bowlerEconomy.push([currentBowler[0],currentBowler[1][0]/currentBowler[1][1]]);
            return bowlerEconomy
        },[]);

        

        bowlersEconomy.sort(function(a, b) {
            return a[1] - b[1];
        });
        let top10BowlersEconomyArray=bowlersEconomy.slice(0,10);
        
        top10BowlersEconomy=top10BowlersEconomyArray.reduce((economyObj,currentBowler)=>{
            economyObj[currentBowler[0]]=currentBowler[1];
            
            return economyObj;
        },{})
        
        return top10BowlersEconomy;
        
    }
    catch(err){
        console.error(err);
    }
}

module.exports = {getNoOfMatchesPerYear,getNoOfWonMatchesPerTeamPerYear,getExtraRunsPerTeam,getTopTenEconomicalBowler};