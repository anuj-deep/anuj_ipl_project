const functionForResult = require('./ipl.js'); 

const csvtojsonV2=require("csvtojson");
const fs=require("fs");
const filepath1="./../data/matches.csv";
const filepath2="./../data/deliveries.csv";
async function saveNoOfMatchesPerYear(){
    try{
        const matchesData=await csvtojsonV2().fromFile(filepath1);
        let noOfMatchesPerYear=functionForResult.getNoOfMatchesPerYear(matchesData);
        noOfMatchesPerYear=JSON.stringify(noOfMatchesPerYear);
        fs.writeFile('./../public/output/matchesPerYear.json',noOfMatchesPerYear,'utf-8',(err)=>
                    {if (err) console.error(err);})
        
    }
    catch(err){
        console.error(err);
    }
}
async function saveNoOfWonMatchesPerTeamPerYear(){
    try{
        const matchesData=await csvtojsonV2().fromFile(filepath1);
        let noOfWonMatchesPerTeamPerYear=functionForResult.getNoOfWonMatchesPerTeamPerYear(matchesData);
        noOfWonMatchesPerTeamPerYear=JSON.stringify(noOfWonMatchesPerTeamPerYear);
        fs.writeFile('./../public/output/wonMatchesPerTeamPerYear.json',noOfWonMatchesPerTeamPerYear,'utf-8',(err)=>
                    {if (err) console.error(err);})
        
    }
    catch(err){
        console.error(err);
    }
}

async function saveExtraRunsPerTeam(){
    try{
        const matchesData=await csvtojsonV2().fromFile(filepath1);
        const deliveriesData=await csvtojsonV2().fromFile(filepath2);
        let extraRunsPerTeam=functionForResult.getExtraRunsPerTeam(matchesData,deliveriesData);
        extraRunsPerTeam=JSON.stringify(extraRunsPerTeam);
        fs.writeFile('./../public/output/extraRunsPerTeam.json',extraRunsPerTeam,'utf-8',(err)=>
                    {if (err) console.error(err);})
    }
    catch(err){
        console.error(err);
    }
}

async function saveTopTenEconomicalBowler(){
    try{
        const matchesData=await csvtojsonV2().fromFile(filepath1);
        const deliveriesData=await csvtojsonV2().fromFile(filepath2);
        let topTenEconomicalBowler=functionForResult.getTopTenEconomicalBowler(matchesData,deliveriesData);
        topTenEconomicalBowler=JSON.stringify(topTenEconomicalBowler);
        fs.writeFile('./../public/output/topTenEconomicalBowler.json',topTenEconomicalBowler,'utf-8',(err)=>
                    {if (err) console.error(err);})
    }
    catch(err){
        console.error(err);
    }
}

function helper() {
    saveNoOfMatchesPerYear();
    saveNoOfWonMatchesPerTeamPerYear();
    saveExtraRunsPerTeam();
    saveTopTenEconomicalBowler();

}

helper();


